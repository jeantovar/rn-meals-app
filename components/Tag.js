import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Ionicons} from '@expo/vector-icons';
import COLORS from '../constants/Colors';


const Tag = props =>{
    return(
        <View style={styles.tagbox}>
            <View style={{ padding: 5}}>
                <Ionicons name='ios-checkmark' color='white' size={36}/>
            </View>
            <View style={{ paddingRight: 10, alignItems: "center", justifyContent: "center"}}>
                <Text style={{fontSize: 14, color: 'white', fontWeight: "bold"}}>{props.children}</Text>
            </View>
            
        </View>
    );
}
const styles = StyleSheet.create({
    tagbox:{
        backgroundColor: COLORS.accent,
        borderRadius: 10,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: {with: 0, height: 2},
        shadowRadius: 10,
        elevation: 3,
        borderColor: COLORS.accent,
        flexDirection: "row"
        
    }
});

export default Tag;
