import React from 'react';
import { StyleSheet, TouchableOpacity, View, Text, Platform, TouchableNativeFeedback } from 'react-native';

const CategoryGridTile = props => {
    
    const TouchableCmp = Platform.OS === 'android' && Platform.Version >= 21 ? TouchableNativeFeedback : TouchableOpacity;
    return (
        <View style={styles.gridItem}>
            <TouchableCmp
                onPress={props.onSelect}
                
            >
                <View style={{...styles.container, ...{backgroundColor: props.obj.color}}}>
                    <Text style={styles.title} numberOfLines={2}>{props.obj.title}</Text>
                </View>
            </TouchableCmp>
        </View>
        );
}

const styles = StyleSheet.create({
    gridItem:{
        margin: 15,
        height: 100,
        justifyContent: "center",
        alignItems: "center"
    },
    container: {
        flex: 1,
        borderRadius: 10,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: {with: 0, height: 2},
        shadowRadius: 10,
        elevation: 3,
        alignItems: "flex-end",
        justifyContent: "flex-end",
        padding: 15,
        minWidth: 130
    },
    title: {
        //fontFamily: 'open-sans-bold',
        fontSize: 18,
        textAlign: "right"
    }
});
export default CategoryGridTile;
