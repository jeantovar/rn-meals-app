import React from 'react';
import { View, Text, ImageBackground, TouchableOpacity, StyleSheet } from 'react-native';
import COLORS from '../constants/Colors';

const MealItem = props => {
    const obj = props.meal;
    return (
        <View style={styles.mealRow}>
            <TouchableOpacity onPress={props.onSelected} style={styles.container}>
                <View>
                    <View style={styles.upper}>
                        <ImageBackground source={{uri: obj.imageUrl}} style={styles.img}>
                            <View style={styles.titleContainer}>
                                <Text style={styles.title} numberOfLines={1}>{obj.title}</Text>
                            </View>
                            
                        </ImageBackground>
                    </View>
                    <View style={styles.bottom}>
                        <Text>{obj.duration}m</Text>
                        <Text>{obj.complexity.toUpperCase()}</Text>
                        <Text>{obj.affordability.toUpperCase()}</Text>
                        
                    </View>
                </View>
                
            </TouchableOpacity>
        </View>
        
    );
}

const styles = StyleSheet.create({

    mealRow:{
        height: 200,
        width: '100%',
        backgroundColor: COLORS.tinte,
        marginVertical: 10
    }  ,  
    img:{
        height: '100%',
        width: '100%',
        justifyContent: "flex-end",
        flex: 1
    }, 
    container:{
        flex: 1,
        width: '100%'
    },
    titleContainer:{
        backgroundColor: 'rgba(0,0,0,0.5)',
        paddingVertical: 10,
        paddingHorizontal: 5
    },
    title: {
        fontSize: 18,
        color: 'white',
        textAlign: "center",
        fontWeight: "bold"
        
    },
    bottom:{
        flexDirection: "row",
        height: "10%",
        backgroundColor: COLORS.tinte,
        justifyContent: "space-between"
    },
    upper: {
        height: "90%"
    }

});

export default MealItem;