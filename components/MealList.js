import React from 'react';
import { View, FlatList, StyleSheet } from 'react-native';
import MealItem from './MealItem';
import { useSelector } from 'react-redux';

const MealList = props => {

    const favMeals = useSelector((state)=>{
        return state.meals.favoriteMeals;
    });

    const onMealSelected = (meal) => {
        console.log('clicked MealItem...');
        const favorite = favMeals.some(ameal => ameal === meal );
        props.navigation.navigate({
            routeName: 'MealDetail',
            params: { 
                obj: meal,
                isFav: favorite 
            }
        });
    }


    const renderMealItem = (itemData) => {
        //console.log(itemData);
        const obj = itemData.item;
        return (

            <MealItem
                meal={obj}
                onSelected={onMealSelected.bind(this, obj)}
            />
        );
    }

    return (
        <View style={styles.list}>
            <FlatList
                data={props.list}
                renderItem={renderMealItem}
                keyExtractor={(item, index) => item.id}
                style={{ flex: 1 }} />

        </View>
    );
}

const styles = StyleSheet.create({
    list: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    }
});

export default MealList;