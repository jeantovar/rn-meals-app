import { MEALS } from '../../data/dummy-data';
import { TOGGLE_FAVORITE, MEAL_FILTERS } from '../actions/meals';

const initialState = {
    meals: MEALS,
    filteredMeals: MEALS,
    favoriteMeals: []
}

const mealsReducer = (state = initialState, action) => {
    console.log("[mealsReducer]");
    switch (action.type) {
        case TOGGLE_FAVORITE:
            const indexMeal = state.favoriteMeals.findIndex(meal => meal === action.meal);
            if (indexMeal >= 0) {
                //removerla
                const updFavMeals = [...state.favoriteMeals];
                updFavMeals.splice(indexMeal, 1);
                return { ...state, favoriteMeals: updFavMeals }
            } else {
                //agergarla
                return { ...state, favoriteMeals: state.favoriteMeals.concat(action.meal) }
            }

        case MEAL_FILTERS:
            const currentFilters = action.currentFilters;
            const upFilteredMeals = state.meals.filter((meal) => {
                if (currentFilters.isVegan && !meal.isVegan) {
                    return false;
                }
                if (currentFilters.isLactoseFree && !meal.isLactoseFree) {
                    return false;
                }
                if (currentFilters.isGlutenFree && !meal.isGlutenFree) {
                    return false;
                }
                if (currentFilters.isVegetarian && !meal.isVegetarian) {
                    return false;
                }
                return true;
            });
            return { ...state, filteredMeals: upFilteredMeals };
        default:
            return state;

    }

}

export default mealsReducer;