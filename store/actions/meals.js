export const TOGGLE_FAVORITE = 'TOGGLE_FAVORITE';
export const MEAL_FILTERS = 'MEALS_FILTERS';

export const togleFavorite = (meal) =>{
    return { type: TOGGLE_FAVORITE, meal: meal }
}

export const mealFilters = (filters) =>{
    return { type: MEAL_FILTERS, currentFilters: filters }
}