import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createDrawerNavigator } from 'react-navigation-drawer'
import { createAppContainer } from 'react-navigation';
import { Platform } from 'react-native';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs'
import CategoriesScreen from '../screens/CategoriesScreen';
import CategoryMealScreen from '../screens/CategoryMealScreen';
import MealDetailScreen from '../screens/MealDetailScreen';
import COLORS from '../constants/Colors';
import FavoritesScreen from '../screens/FavoritesScreen';
import FiltersScreen from '../screens/FiltersScreen';
import { Ionicons } from '@expo/vector-icons';

const defStackConfig = {
    headerTitle: 'No Title',
    headerStyle: {
        backgroundColor: Platform.OS === 'android' ? COLORS.primary : ''
    },
    headerTitleStyle:{
        fontFamily: Platform.OS === 'android' ?  'monospace' : 'System',
        fontWeight: "bold"
    },
    headerBackStyle: {
        fontFamily: Platform.OS === 'android' ?  'monospace' : 'System'
    },
    headerTintColor: Platform.OS === "android" ? 'white' : COLORS.primary
};

const MealsNavigator = createStackNavigator({

    Categories: {
        screen: CategoriesScreen,
        navigationOptions: {
            headerTitle: 'Meals Categories'
        }
    },
    CategoryMeals: {
        screen: CategoryMealScreen
    },
    MealDetail: MealDetailScreen
},
    //este segundo Objeto argumento son las Configuraciones comunes
    {
        defaultNavigationOptions: defStackConfig,
        //si queremos cambiar la pantalla de Inicio usamos  :
        initialRouteName: 'Categories',   // (opcional, por defecto toma la primera de la lista de navegacion)
        mode: "modal"
    }
);

const FavsNavigator = createStackNavigator({
    Favorites: FavoritesScreen,
    MealDetail: MealDetailScreen
}, {
    defaultNavigationOptions: defStackConfig
});

const FilterNav = createStackNavigator({
    FilterMeal : FiltersScreen
    
    },{
    defaultNavigationOptions: defStackConfig
});

const commTabSettings = {
    Meals: {
        screen: MealsNavigator,
        navigationOptions: {
            tabBarIcon: (tabInfo) => {
                return <Ionicons name='ios-restaurant' color={tabInfo.tintColor} size={25} />
            }
        }
    },
    Favorites: {
        screen: FavsNavigator,
        navigationOptions: {
            tabBarLabel: 'Favorites!!',
            tabBarIcon: (tabInfo) => {
                return <Ionicons name='ios-star' color={tabInfo.tintColor} size={25} />
            }
        }
    }
};

const MealsAndFavsNavigator = (Platform.OS === 'android')
    ? createMaterialBottomTabNavigator(commTabSettings,
        {
            activeColor: COLORS.primary,
            shifting: true,
            inactiveColor: 'gray',
            barStyle: { backgroundColor: COLORS.tinte }
        }
    )
    : createBottomTabNavigator(commTabSettings,
        {
            tabBarOptions: {
                activeTintColor: COLORS.primary,

            }
        });

const MainNavigator = createDrawerNavigator(
    {
        MealsFavs : MealsAndFavsNavigator,
        Filters : FilterNav
    }
);        
export default createAppContainer(MainNavigator);