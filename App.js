import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import MealsNavigator from './navigation/MealsNavigator';
//usar para mejorar el rendimiento de las Pantallas
import { useScreens } from 'react-native-screens';

import * as Font from 'expo-font';
import { AppLoading } from 'expo';

//REDUX
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';

import mealsReducer from './store/reducers/meals';


useScreens();  //invocar antes de cualquier cosa, para optimizar

const rootReducer = combineReducers({
  meals: mealsReducer
});

const store = createStore(rootReducer);

const fetchFonts = () => {
  console.log('trying to load fonts...');
  return Font.loadAsync({
    'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
    'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf')
  });
}

export default function App() {

  const [fontLoaded, setFontLoaded] = useState(false);

  if (!fontLoaded) {
    <AppLoading
      startAsync={fetchFonts}
      onFinish={() => {
        setFontLoaded(true);
        console.log('fonts LOADED...');
      }
      }
      onError={err => console.log(err)}
    />
  }
  return (
    <Provider store={store}>
      <MealsNavigator style={styles.container} />
    </Provider>
    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
