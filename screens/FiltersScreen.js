import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Switch, Platform } from 'react-native';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import HeaderBttn from '../components/HeaderBttn';
import COLORS from '../constants/Colors';

import { useDispatch } from 'react-redux';
import { mealFilters } from '../store/actions/meals';

const MealSwitch = props => {

    return (
        <View style={styles.switchBox}>
            <Text style={styles.switchText}>{props.label}</Text>
            <Switch value={props.value}
                onValueChange={props.onChange}
                trackColor={{ true: COLORS.primary }}
                thumbColor={Platform.OS === 'android' ? COLORS.primary : ''} />
        </View>
    );
}

const FiltersScreen = props => {

    const [isGlutenFree, setIsGlutenFree] = useState(false);
    const [isLactoseFree, setIsLactoseFree] = useState(false);
    const [isVegan, setIsVegan] = useState(false);
    const [isVegetarian, setIsVegetarian] = useState(false);

    const dispatch = useDispatch();

    useEffect(()=>{
        const currentFilters={
            isGlutenFree: isGlutenFree,
            isLactoseFree: isLactoseFree,
            isVegan: isVegan,
            isVegetarian: isVegetarian
        };
        dispatch(mealFilters(currentFilters));
    },[isVegetarian, isVegan, isLactoseFree, isGlutenFree, dispatch]);

    return (
        <View style={styles.screen}>
            <Text style={styles.title}>Filters and Restrictions</Text>
            <MealSwitch label="Lactose Free"
                value={isLactoseFree}
                onChange={(newVal)=>{setIsLactoseFree(newVal)}} />
            <MealSwitch label="Gluten Free"
                value={isGlutenFree}
                onChange={(newVal)=>{setIsGlutenFree(newVal)}} />
            <MealSwitch label="Vegan"
                value={isVegan}
                onChange={(newVal)=>{setIsVegan(newVal)}} />
            <MealSwitch label="Vegetarian"
                value={isVegetarian}
                onChange={(newVal)=>{setIsVegetarian(newVal)}} />
        </View>
    );
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        alignItems: "center"
    },
    title: {
        fontWeight: "bold",
        fontSize: 22,
        color: COLORS.accent,
        textAlign: "center",
        marginVertical: 15
    },
    switchBox:{
        flexDirection: "row",
        width: '80%',
        justifyContent: "space-between",
        alignItems: "center",
        margin: 15
    },
    switchText:{
        fontWeight: "bold",
        color: 'black',
        fontSize: 18
    }
});

FiltersScreen.navigationOptions = navData => {
    return ({
        headerTitle: 'Filter Meals',
        headerLeft: <HeaderButtons HeaderButtonComponent={HeaderBttn}>
            <Item title='favorite' iconName='ios-menu'
                onPress={() => {
                    console.log('Menu pressed..');
                    navData.navigation.toggleDrawer()
                }} />
        </HeaderButtons>
    }

    );



}

export default FiltersScreen;