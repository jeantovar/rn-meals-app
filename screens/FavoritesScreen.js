import React from 'react';
import { View,  StyleSheet } from 'react-native';
import MealList from '../components/MealList';
//import { MEALS } from '../data/dummy-data';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import HeaderBttn from '../components/HeaderBttn';

import { useSelector } from 'react-redux';



const FavoritesScreen = props => {

    const FAV_MEAL = useSelector(state => state.meals.favoriteMeals);
    return (
        <View style={styles.screen}>
            <MealList list={FAV_MEAL} navigation={props.navigation} />
        </View>
    );
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    }
});

FavoritesScreen.navigationOptions = navData =>{
    return({
        headerTitle: 'Your Favorites',
        headerLeft: <HeaderButtons HeaderButtonComponent={HeaderBttn}>
            <Item title='favorite' iconName='ios-menu' 
                                onPress={()=> {
                                        console.log('Menu pressed..');
                                        navData.navigation.toggleDrawer()
                                        }}/>
        </HeaderButtons>
    }
        
    );
    


}

export default FavoritesScreen;