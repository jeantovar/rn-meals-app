import React from 'react';
import { View, StyleSheet, Button, FlatList } from 'react-native';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import HeaderBttn from '../components/HeaderBttn';

import { CATEGORIES } from '../data/dummy-data';
import CategoryGridTile from '../components/CategoryGridTile';


const CategoriesScreen = props =>{

    
    //console.log(props);
    const renderGridItem = (itemData) =>{
        return (
            
            <CategoryGridTile 
                obj={itemData.item} 
                onSelect={()=>{
                        props.navigation.navigate({
                            routeName: 'CategoryMeals',
                            params: {
                                category: itemData.item
                            }
                        })   
                    }
                }
            />
        );
    }

    return(
        <View style={styles.screen}>
            <FlatList data={CATEGORIES}  
                        keyExtractor={(item, index) => item.id}
                        renderItem={renderGridItem}
                        numColumns={2}/>
            <Button title="Go to Category Meal" onPress={() => {
                props.navigation.navigate({routeName: 'CategoryMeals'});
            }}/>
        </View>
    );
}

//todas las funciones JSX son Objetos, por lo que podemos agregarles propiedades como sigue:

CategoriesScreen.navigationOptions = navData =>{
    return({
        headerLeft: <HeaderButtons HeaderButtonComponent={HeaderBttn}>
            <Item title='favorite' iconName='ios-menu' 
                                onPress={()=> {
                                        console.log('Menu pressed..');
                                        navData.navigation.toggleDrawer()
                                        }}/>
        </HeaderButtons>
    }
        
    );
    


}



const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    }
    
});

export default CategoriesScreen;