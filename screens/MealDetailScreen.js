import React, { useEffect, useCallback } from 'react';
import { ScrollView, Image, View, Text, StyleSheet, Button } from 'react-native';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import HeaderBttn from '../components/HeaderBttn';
import Tag from '../components/Tag';
import { useDispatch, useSelector } from 'react-redux';

import COLORS from '../constants/Colors';
import { togleFavorite } from '../store/actions/meals';

const RowItem = props => {
    return (
        <View style={styles.rowItemBox}>
            <Text>{props.children}</Text>
        </View>
    );
}

const MealDetailScreen = props => {



    const myObj = props.navigation.getParam('obj');

    const isCurrFavMeals = useSelector(state =>{ 
        console.log("[MealDetailScreen.js] isCurrFavMeals");
        return state.meals.favoriteMeals.some(meal=> myObj.id === meal.id);
    });
    //console.log(myObj);

    const toggleFavsHandler = useCallback(() => {
        dispatch(togleFavorite(myObj));
    },[dispatch, myObj]);

    useEffect(()=>{
        props.navigation.setParams({toggleFav: toggleFavsHandler});
    },[toggleFavsHandler]);

    useEffect(()=>{
        props.navigation.setParams({isFav: isCurrFavMeals})
    },[isCurrFavMeals]);

    const buildTags = (condition, message) => {
        if (condition === true) {
            return <Tag>{message}</Tag>
        }
        return;
    }

    const dispatch = useDispatch();

    

    return (
        <ScrollView>
            <View style={styles.summary}>
                {buildTags(myObj.isVegan, 'Vegan')}
                {buildTags(myObj.isVegetarian, 'Vegetarian')}
                {buildTags(myObj.isGlutenFree, 'Gluten Free')}
                {buildTags(myObj.isLactosaFree, 'Lactosa Free')}
            </View>
            <Image source={{ uri: myObj.imageUrl }} style={styles.img} />
            <View style={styles.summary}>
                <Text style={{ fontWeight: "bold" }}>{myObj.duration}m</Text>
                <Text style={{ fontWeight: "bold" }}>{myObj.complexity.toUpperCase()}</Text>
                <Text style={{ fontWeight: "bold" }}>{myObj.affordability.toUpperCase()}</Text>
            </View>
            <View>
                <Text style={styles.title}>INGREDIENTS</Text>
                {myObj.ingredients.map((ingred, index) => {
                    return <RowItem key={index}>{ingred}</RowItem>
                })}
            </View>
            <View>
                <Text style={styles.title}>STEPS</Text>
                {myObj.steps.map((ingred, index) => {
                    return <RowItem key={index}>{ingred}</RowItem>
                })}
            </View>
            <Button title="Go Back To Categories"
                onPress={() => {
                    props.navigation.popToTop();
                }
                }
            />

        </ScrollView>

    );
}

const styles = StyleSheet.create({
    summary: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: "space-between",
        paddingHorizontal: 15,
        paddingVertical: 10,
        backgroundColor: COLORS.tinte

    },
    screen: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    img: {
        height: 300,
        width: '100%'
    },
    title: {
        fontSize: 34,
        fontWeight: "bold",
        color: COLORS.accent,
        textAlign: "center",
        marginVertical: 10
    },
    rowItemBox: {
        borderWidth: 1,
        borderColor: COLORS.accent,
        marginHorizontal: 10,
        marginVertical: 10,
        paddingVertical: 15,
        paddingHorizontal: 10

    }

});

MealDetailScreen.navigationOptions = (navData) => {
    const screenHeader = navData.navigation.getParam('obj').title;
    const favHandler = navData.navigation.getParam('toggleFav');
    const isFav = navData.navigation.getParam('isFav');
    return {
        headerTitle: screenHeader,
        headerRight: <HeaderButtons HeaderButtonComponent={HeaderBttn}>
            <Item title='favorite' iconName= {isFav ? 'ios-star' : 'ios-star-outline' } onPress={ favHandler } />
        </HeaderButtons>
    }
}

export default MealDetailScreen;