import React from 'react';

//import { MEALS } from '../data/dummy-data';
import MealList from '../components/MealList';

import { useSelector } from 'react-redux';

const CategoryMealScreen = props => {

    const myCategory = props.navigation.getParam('category');
    const availableMeals = useSelector(state => state.meals.filteredMeals);

    const displayedMeals = availableMeals.filter(meal => meal.categoryIds.indexOf(myCategory.id) >= 0);

    return (
        <MealList list={displayedMeals} navigation={props.navigation}/>
    );
}

//le agregamos propiedades a la navegacion, pero esta vez usando una funcion!!
CategoryMealScreen.navigationOptions = navData => {
    const myCategory = navData.navigation.getParam('category');
    return {
        headerTitle: myCategory.title,

    };
};


export default CategoryMealScreen;