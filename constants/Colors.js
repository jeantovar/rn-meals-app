export default {
    primary: '#2DB5EC',
    accent: '#EC642D',
    optional: '#B5EC2D',
    tinte: '#CAECFA'
}